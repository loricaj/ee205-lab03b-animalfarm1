###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

TARGET:i animalfarm

all: $(TARGET)

animalfarm:
	$(info You need to write your own Makefile)
	$(info I know you can do it)
	$(info for now type gcc -o animalfarm *.c)

CC = gcc
CFLAGS = -g

main.o: main.c animals.h cat.h
	$(CC) $(CFLAGS) -c main.c

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c

animalfarm: animals.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o animals.o cat.o

clean:
	rm -f *.o animalfarm
