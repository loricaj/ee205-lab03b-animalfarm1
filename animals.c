///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2/2/21
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   switch(color){
      case BLACK : return "Black"; 
      case WHITE : return "White";
      case RED   : return "Red";
      case BLUE  : return "Blue";  
      case GREEN : return "Green"; 
      case PINK  : return "Pink"; 
      default: break;
   }

   /*
   if(color==BLACK)
      return "Black";
   else if (color==RED)
      return "Red";
   */

   return NULL; // We should never get here
};

char* genderName (enum Gender gender){
   if(gender = MALE)
      return "Male";
   else
      return "Female";
}
