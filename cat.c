///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2/2/21
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat myCats[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   myCats[i];    //initialize Alice the cat

   strcpy(myCats[i].name, "Alice" );
   myCats[i].gender = FEMALE;
   myCats[i].breed = MAIN_COON;
   myCats[i].isFixed = true;
   myCats[i].weight = 12.34;
   enum Color collar1_color = BLACK;
   enum Color collar2_color = RED; 
   myCats[i].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like...
   printf ("Cat name = [%s]\n", myCats[i].name);
   printf ("    gender = [%s]\n ", genderName( myCats[i].gender ));
   printf ("   breed = [%s]\n", breedName( myCats[i].breed ));
   printf ("    isFixed = [%s]\n", fixedName( myCats[i].isFixed ));
   printf ("    weight = [%2.2f]\n", myCats[i].weight);
   printf ("    collar color 1 = [%s]\n", colorName( myCats[i].collar1_color ));
   printf ("    collar color 2 = [%s]\n", colorName( myCats[i].collar2_color ));
   printf ("    license = [%d]\n", myCats[i].license);
}

char* breedName (enum CatBreeds breed){
   switch(breed){
      case MAIN_COON: return "Main Coon";
      case MANX:      return "Manx";
      case SHORTHAIR: return "Shorthair";
      case PERSIAN:   return "Persian";
      case SPHYNX:    return "Sphynx";
   }
}

char* fixedName (bool fixed){
   if(fixed)
      return "Yes";
   else
      return "No";
}
